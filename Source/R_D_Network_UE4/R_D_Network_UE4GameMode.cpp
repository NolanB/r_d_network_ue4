// Copyright Epic Games, Inc. All Rights Reserved.

#include "R_D_Network_UE4GameMode.h"
#include "R_D_Network_UE4Character.h"
#include "UObject/ConstructorHelpers.h"

AR_D_Network_UE4GameMode::AR_D_Network_UE4GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
