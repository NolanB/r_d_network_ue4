// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "R_D_Network_UE4GameMode.generated.h"

UCLASS(minimalapi)
class AR_D_Network_UE4GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AR_D_Network_UE4GameMode();
};



