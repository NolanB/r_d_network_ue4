// Fill out your copyright notice in the Description page of Project Settings.


#include "RoomConstruction.h"

// Sets default values
ARoomConstruction::ARoomConstruction()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARoomConstruction::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARoomConstruction::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

