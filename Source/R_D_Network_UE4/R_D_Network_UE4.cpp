// Copyright Epic Games, Inc. All Rights Reserved.

#include "R_D_Network_UE4.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, R_D_Network_UE4, "R_D_Network_UE4" );
 