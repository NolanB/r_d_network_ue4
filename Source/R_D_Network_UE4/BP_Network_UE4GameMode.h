// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "R_D_Network_UE4GameMode.h"
#include "BP_Network_UE4GameMode.generated.h"

/**
 * 
 */
UCLASS()
class R_D_NETWORK_UE4_API ABP_Network_UE4GameMode : public AR_D_Network_UE4GameMode
{
	GENERATED_BODY()
	
};
